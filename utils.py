import numpy as np
import torch

class ReplayBuffer:

    def __init__(self, state_size, action_size, capacity, batch_size):
        self.state_size = state_size
        self.action_size = action_size
        self.capacity = capacity
        self.batch_size = batch_size
        self.clear()

    def append(self, state, action, reward, next_state):
        idx = self.ctr % self.capacity
        self.states[idx] = state
        self.actions[idx] = action
        self.rewards[idx] = reward
        self.next_states[idx] = next_state
        self.ctr += 1

    def get_batch(self):
        if self.ctr < self.batch_size:
            idxs = np.arange(self.ctr)
        elif self.ctr < self.capacity:
            idxs = np.random.choice(self.ctr, self.batch_size, replace=False)
        else:
            idxs = np.random.choice(self.capacity, self.batch_size, replace=False)
        states = torch.tensor(self.states[idxs], dtype=torch.float32)
        actions = torch.tensor(self.actions[idxs], dtype=torch.float32)
        rewards = torch.tensor(self.rewards[idxs], dtype=torch.float32)
        next_states = torch.tensor(self.next_states[idxs], dtype=torch.float32)
        return states, actions, rewards, next_states

    def clear(self):
        self.states = np.zeros((self.capacity, self.state_size))
        self.actions = np.zeros((self.capacity, self.action_size))
        self.rewards = np.zeros((self.capacity, 1))
        self.next_states = np.zeros((self.capacity, self.state_size))
        self.ctr = 0


def reset_env(env, init_state):
    observation = env.reset()  # set random init state
    if (init_state is not None):  # set a fix init state if not None
        env = env.unwrapped
        env.state = init_state
        observation = init_state
    return observation


def reset_init_state(env):
    init_state = env.reset()
    return init_state