# Soft Actor Critic as Inference

This repository accompanies the report entitled "Interpretation and
Implementation of Soft Actor Critic as Probabilistic Inference", which
was submitted for the Winter 2021 delivery of CS842.

The experiments described in the report are implemented here. Each
Jupyter Notebook contains code to run a different experiment. All
expiments were implemented using Python and Pyro. Below is a list of the
notebooks and a brief description of each:
- *[cartpole_mepg.ipynb](cartpole_mepg.ipynb)*: Probabilistic inference
  formulation of Maximum Entropy Policy Gradient (MEPG) for the
  CartPole-v1 environment
- *[cartpole_sac.ipynb](cartpole_sac.ipynb)*: Probabilistic inference formulation of Soft
  Actor Critic (SAC) for the CartPole-v1 environment
- *[pendulum_mepg.ipynb](pendulum_mepg.ipynb)*: Probabilistic inference formulation of MEPG
  for the Pendulum-v0 environment
- *[cartpole_sac.ipynb](cartpole_sac.ipynb)*: Probabilistic inference formulation of SAC
  for the Pendulum-v0 environment
- *[mountaincar_sac.ipynb](mountaincar_sac.ipynb)*: Probabilistic inference formulation of
  SAC for the MountainCar-v0 environment
- *[sac_baseline.ipynb](sac_baseline.ipynb)*: Baseline implementation of SAC, conducted
  for Pendulum-v0

I wish to express my gratitude to Frank Shi and Professor Yizhou Zhang
for their prior work in RL-as-inference in probabilistic programming.
[Their repository](https://github.com/frankmao666/pyro-nn) was an
excellent inspiration and starting point for this project.

